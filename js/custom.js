var openSignInModal = document.getElementById("open-sign-in-modal");
openSignInModal.onclick = function() {
    var modal = document.getElementById("sign-in-modal");
    openModal(modal)
}

var openSignUpModal = document.getElementById("open-sign-up-modal");
openSignUpModal.onclick = function() {
    var modal = document.getElementById("sign-up-modal");
    openModal(modal)
}

function openModal(modalElement) {
    closeOpenedModals()
    modalElement.style.display = "block";
    modalElement.classList.add("modal-opened");
}

function closeModal(closeElement) {
    var modal = closeElement.parentElement.parentElement;
    modal.style.display = "none";
    modal.classList.remove("modal-opened");
}

function closeOpenedModals() {
    var modals = document.getElementsByClassName("modal-opened");

    for (const modal of modals) {
        modal.style.display = "none";
        modal.classList.remove("modal-opened");
    }
}



window.onclick = function(event) {
    var modals = document.getElementsByClassName("modal-opened");

    for (const modal of modals) {
        if (event.target === modal) {
            modal.style.display = "none";
            modal.classList.remove("modal-opened");
        }
    }
}